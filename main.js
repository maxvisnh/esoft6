import { checkBoard } from "./checkBoard.js";

let currentPlayer =document.getElementById('curPlay');

class GameField {
    state = [                // state - информация о клетках поля, изначально все клетки пустые;
        ['', '', ''],
        ['', '', ''],
        ['', '', '']
        ];
    mode = "x";               //mode - свойство для хранения информации активного игрока (крестики или нолики), первые ходят крестики;
    setMode(){              //setMode - метод для изменения активного игрока;
        switch (this.mode){
            case "x":
                this.mode = "o";
                break;
            case "o":
                this.mode = "x";
                break;
        }
    }
    getGameFieldStatus(){   //getGameFieldStatus - метод для получения информации о состоянии игрового поля;
        console.log(this.state, this.mode, this.isOverGame);
    }
}
const gameField = new GameField();
console.log(gameField);
function cellClick(){
    if(!this.innerHTML) {
        this.innerHTML = gameField.mode;
        gameField.setMode();
        currentPlayer.innerHTML = gameField.mode;
        
        checkBoard(gameField.state);
        console.log(gameField.state);
    }
    else {
        alert("Ячейка занята");
        return;
    }
}

let inputClick1 =document.getElementById("cell1");
let inputClick2 =document.getElementById("cell2");
let inputClick3 =document.getElementById("cell3");
let inputClick4 =document.getElementById("cell4");
let inputClick5 =document.getElementById("cell5");
let inputClick6 =document.getElementById("cell6");
let inputClick7 =document.getElementById("cell7");
let inputClick8 =document.getElementById("cell8");
let inputClick9 =document.getElementById("cell9");

let empty1 = 0;
let empty2 = 0;
let empty3 = 0;
let empty4 = 0;
let empty5 = 0;
let empty6 = 0;
let empty7 = 0;
let empty8 = 0;
let empty9 = 0;

inputClick1.onclick= function(){
    switch (empty1){
        case 0:
            switch (gameField.mode){
                case "x":
                    this.innerHTML="x";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty1=1;
                    gameField.state[0][0] = "x";
                    break;
                case "o":
                    this.innerHTML="o";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty1=1;
                    gameField.state[0][0] = "o";
                    break; 
                }
            break;
        case 1:
            alert("Занято");
            break;
        }
    console.log(gameField.state);
    checkBoard(gameField.state);
}

inputClick2.onclick= function(){
    switch (empty2){
        case 0:
            switch (gameField.mode){
                case "x":
                    this.innerHTML="x";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty2=1;
                    gameField.state[0][1] = "x";
                    break;
                case "o":
                    this.innerHTML="o";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty2=1;
                    gameField.state[0][1] = "o";
                    break; 
                }
            break;
        case 1:
            alert("Занято");
            break;
        }
    console.log(gameField.state);
    checkBoard(gameField.state);
}

inputClick3.onclick= function(){
    switch (empty3){
        case 0:
            switch (gameField.mode){
                case "x":
                    this.innerHTML="x";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty3=1;
                    gameField.state[0][2] = "x";
                    break;
                case "o":
                    this.innerHTML="o";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty3=1;
                    gameField.state[0][2] = "o";
                    break; 
                }
            break;
        case 1:
            alert("Занято");
            break;
        }
    console.log(gameField.state);
    checkBoard(gameField.state);
}

inputClick4.onclick= function(){
    switch (empty4){
        case 0:
            switch (gameField.mode){
                case "x":
                    this.innerHTML="x";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty4=1;
                    gameField.state[1][0] = "x";
                    break;
                case "o":
                    this.innerHTML="o";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty4=1;
                    gameField.state[1][0] = "o";
                    break; 
                }
            break;
        case 1:
            alert("Занято");
            break;
        }
    console.log(gameField.state);
    checkBoard(gameField.state);
}

inputClick5.onclick= function(){
    switch (empty5){
        case 0:
            switch (gameField.mode){
                case "x":
                    this.innerHTML="x";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty5=1;
                    gameField.state[1][1] = "x";
                    break;
                case "o":
                    this.innerHTML="o";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty5=1;
                    gameField.state[1][1] = "o";
                    break; 
                }
            break;
        case 1:
            alert("Занято");
            break;
        }
    console.log(gameField.state);
    checkBoard(gameField.state);
}

inputClick6.onclick= function(){
    switch (empty6){
        case 0:
            switch (gameField.mode){
                case "x":
                    this.innerHTML="x";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty6=1;
                    gameField.state[1][2] = "x";
                    break;
                case "o":
                    this.innerHTML="o";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty6=1;
                    gameField.state[1][2] = "o";
                    break; 
                }
            break;
        case 1:
            alert("Занято");
            break;
        }
    console.log(gameField.state);
    checkBoard(gameField.state);
}

inputClick7.onclick= function(){
    switch (empty7){
        case 0:
            switch (gameField.mode){
                case "x":
                    this.innerHTML="x";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty7=1;
                    gameField.state[2][0] = "x";
                    break;
                case "o":
                    this.innerHTML="o";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty7=1;
                    gameField.state[2][0] = "o";
                    break; 
                }
            break;
        case 1:
            alert("Занято");
            break;
        }
    console.log(gameField.state);
    checkBoard(gameField.state);
}

inputClick8.onclick= function(){
    switch (empty8){
        case 0:
            switch (gameField.mode){
                case "x":
                    this.innerHTML="x";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty8=1;
                    gameField.state[2][1] = "x";
                    break;
                case "o":
                    this.innerHTML="o";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty8=1;
                    gameField.state[2][1] = "o";
                    break; 
                }
            break;
        case 1:
            alert("Занято");
            break;
        }
    console.log(gameField.state);
    checkBoard(gameField.state);
}

inputClick9.onclick= function(){
    switch (empty9){
        case 0:
            switch (gameField.mode){
                case "x":
                    this.innerHTML="x";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty9=1;
                    gameField.state[2][2] = "x";
                    break;
                case "o":
                    this.innerHTML="o";
                    gameField.setMode();
                    currentPlayer.innerHTML = gameField.mode;
                    empty9=1;
                    gameField.state[2][2] = "o";
                    break; 
                }
            break;
        case 1:
            alert("Занято");
            break;
        }
    console.log(gameField.state);
    checkBoard(gameField.state);
}